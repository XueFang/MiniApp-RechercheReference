<?php

namespace MagasinBundle\Services;

Class Database
{

    private static $dbSrvc = null;

    private function __construct ()
    {

    }

    public static function getInstance (): Database
    {
        if ( is_null ( self::$dbSrvc ) )
        {
            self::$dbSrvc = new Database();
        }
        return self::$dbSrvc;
    }

    private function startConnect (): \PDO
    {
        try
        {
            $pdo = new \PDO ( sprintf ( "mysql:dbname=%s;host=%s", cDATABASE, cHOST ), cLOGIN, cPASSWORD );
            return $pdo;
        } catch ( \Exception $ex )
        {
            throw new \Exception ( __CLASS__ . '(' . __FUNCTION__ . ') : ' . $ex );
        }
    }

    /**
     * @Execute une requête
     * @param string $rq
     * @param array  $params Tableau de paramètres [ :paramName, value ]
     */
    public static function sendRequest ( string $rq, array $params = null )
    {
        if ( is_null ( $rq ) )
        {
            throw new \Exception ( __CLASS__ . '(' . __FUNCTION__ . ') : "rq" est non déterminé' );
        }
        $request = self::$dbSrvc->startConnect ()->prepare ( $rq );
        if ( !is_null ( $params ) )
        {
            foreach ( $params as $key => $value )
            {
                $request->bindParam ( $key, $value );
            }
        }
        $request->execute ();
        $result = $request->fetchAll ( \PDO::FETCH_ASSOC );
        $request->closeCursor ();
        return $result;
    }

}
