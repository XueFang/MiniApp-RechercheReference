<?php declare(strict_types = 1); ?>
<!DOCTYPE>
<html>
    <head>
        <title>Mini application - Gestion magasin informatique</title>

        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link href="https://fonts.googleapis.com/css?family=Barlow+Condensed" rel="stylesheet">
        <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/default.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
        require_once __DIR__ . '/../core/kernel.php';

        use MagasinBundle\Model\Category as Category;
        use MagasinBundle\Model\Item as Item;

$searchedItem = "ROG GL 703";
        ?>
        <div id='app-research' class='col-xs-offset-1 col-xs-10'>
            <div class='jumbotron'>
                <h2>Recherche de la catégorie de l'item <b><?php echo "<span>" . $searchedItem . "</span>"; ?></b></h2>
                <?php
                $item1 = new Item();
                $item1->setName ( $searchedItem );
                $search = Category::findItemCategory ( $item1 );
                ?>
                <p>L'item se trouve dans la catégorie : <?php
                    if ( count ( $search ) > 0 )
                    {
                        echo $search[0]["Category"];
                    } else
                    {
                        echo "- Aucune catégorie trouvée pour cet item -";
                    }
                    ?>
                </p>
            </div>
        </div>
        <script src="../bower_components/jquery/dist/jquery.min.js"></script>
        <script src="../bower_components/gsap/src/minified/easing/EasePack.min.js"></script>
        <script src="../bower_components/gsap/src/minified/TweenLite.min.js"></script>
        <script src="js/default.js"></script>
    </body>
</html>