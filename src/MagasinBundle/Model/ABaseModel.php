<?php

namespace MagasinBundle\Model;

abstract Class ABaseModel
{

    private $name = null;

    /**
     * @description Retourne "name"
     * @return string
     */
    public function getName (): string
    {
        return $this->name;
    }

    /**
     * @description Gère le nom de l'utilisateur
     * @param string $name
     * @return bool | true : Assignation avec succès, false : Erreur
     */
    public function setName ( string $name ): bool
    {
        if ( is_null ( $name ) )
        {
            throw new \Exception ( __CLASS__ . '(' . __FUNCTION__ . ') : "name" n\'est pas déterminé' );
        }

        $this->name = $name;
        return true;
    }

}
