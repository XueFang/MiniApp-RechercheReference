<?php

namespace MagasinBundle\Model;

use MagasinBundle\Services\Database as Database;

abstract Class Category extends ABaseModel
{

    /**
     *
     * @param \MagasinBundle\Model\Item $item
     * @return array |  Résultat(s) de la requête
     */
    public static function findItemCategory ( Item $item ): array
    {
        $srvc = Database::getInstance ();
        return $srvc::sendRequest (
                        "SELECT category.name AS Category
            FROM category
            JOIN item ON category.id = item.id
            WHERE item.name=:name"
                        , [
                    ":name" => $item->getName ()
                ] );
    }

}
